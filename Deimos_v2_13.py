#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Jan 17 19:36:32 2019

@author: Joaquin Carrasco Palazon (joamet@tutanota.com)
"""
####################################################################################################################################
#                                                              DEIMOS                                                              #
#                                                             CHANGELOG                                                            #
#                                                             09/02/2019                                                           #
# lr = 0.00025                                                                                                                     #
# Cambio de optimizador de Adam a RMSEProp                                                                                         #
# replay_sample_size = de 48 a 32                                                                                                  #
# Actualizacion de la red objetivo cada 10000 pasos en vez de cada 1000                                                            #                                                                                                                                  
#                                                             10/02/2019                                                           #
# Vuelta a Adam, ya que con RMSEProp los resultados obtenidos no son satisfactorios                                                #                                                                                                                                  
#                                                             11/02/2019                                                           #
# Reajuste de lr = 0.00001 a 0.00025 y Actualizacion a 1000 de nuevo, ya que                                                       #
# los resultados no son buenos con los parametros anteriores                                                                       #
# Ante los malos resultados obtenidos al final del entrenamiento con boxing,                                                       #
# se determina que todas las recompensas negativas seran tomadas como 0                                                            #
# Reajuste de lr = 0.0025 a lr = 0.001, segun los datos de el siguiente                                                            #
# experimento: https://medium.com/octavian-ai/which-optimizer-and-learning-rate-should-i-use-for-deep-learning-5acb418f9b2         #
# Añadido plot del loss, para mejor monitorizacion del entrenamiento                                                               #
#                                                            12/02/2019                                                            #
# Añadida pequeña funcion que calcula el valor medio de cada sesion de entrenamiento                                               #
# Añadido plot de la recompensa media para tener un mejor control de la misma                                                      #
# Reajustado el valor minimo de epsilon de 0.01 a 0.1                                                                              #
#                                                            13/02/2019                                                            #
# Añadido el valor maximo de la media, asi como el numero de pasos totales para tener un mejor control del entrenamiento.          #                                                                                                                  
# Añadido que cuando pierda una vida, cuente como que done=True, para "penalizar" de alguna manera la perdida de una vida. Basado  #
# en: https://github.com/fg91/Deep-Q-Learning/blob/master/DQN.ipynb                                                                #
#                                                            14/02/2019                                                            #
# Modificado el sistema de guardado, para que cree una carpeta por juego, para tener todo mejor ordenado.                          #
# Añadidos colores para la salida por pantalla, para un mejor visionado de los datos.                                              #
#                                                            16/02/2019                                                            #
# Cambio del tamaño de memoria de 50000 a 100000, ya que aun podemos ocupar mas ram.                                               #
# Cambio de la version de Space Invaders v0 a la v4, ya que es mas actual.                                                         #
#                                                            18/02/2019                                                            #
# Implementacion del sistema de grabado con shelve para poder guardar las experiencias de memoria almacenadas.                     #
#                                                            19/02/2019                                                            #
# Transicion al nuevo sistema de grabado: almacenamiento de las variables en una base de datos para su posterior carga. Al hacerlo #
# de este modo, eliminamos la necesidad de varios archivos, conjuntandolo todo en uno (y los pesos de las neuronas, que van aparte)#
# ademas de permitir hacer ciertas modificaciones al programa. Actualmente grabara en los dos sistemas para comprobar si funciona  #
# correctamente.                                                                                                                   #
# Cambio del lr = 0.0001 a lr = 0.00125; tras mirar la grafica, parece un punto mas optimo.                                        #
#                                                            21/02/2019                                                            #
# Añadido que cuando pierda una vida, todas las futuras recopmensas sean 0, ya que parece que esto hace que evite morir.           #
# Renombrado de algunos parametros; gamma (antes decay_rate) pasa de valer 0.99 a 0.9.                                             #
#                                                            23/02/2019                                                            #
# Cambio lr de 0.00125 a 0.00025                                                                                                   #
# Modificada funcion action_value, ya que parece ser el origen del problema.                                                       #
#                                                            24/02/2019                                                            #
# Gamma de 0.9 a 0.95                                                                                                              #
# Modificado el done == True por False en la funcion de entrenamiento, ya que estaba mal.                                          #
#                                                            05/03/2019                                                            #
# Modificado el programa para que cuando el valor de las vidas sea < 3 lo tome como que ha terminado; ajustado al numero de vidas  #
# maximo de cada juego, para hacerlo lo mas polivalente posible.                                                                   #
#                                                           07/03/2019                                                             #
# Modificada la carga de experiencias en el entrenamiento; ahora introduce batches de 4 experiencias aleatorias no consecutivas;   #
# en teoria es como deberia de hacerse.                                                                                            #
# El programa ahora no empezara a entrenar hasta que lleve unos 50 episodios.                                                      #
#                                                          08/03/2019                                                              #
# Cambiado la actualizacion de la red objetivo de 1000 pasos a 10000 como en el paper de DeepMind.                                 #
# Cambiado gamma de 0.95 a 0.99 como el paper de DeepMind.                                                                         #
# Modificado el decay de epsilon para que ahora el programa vea 1 millon de experiencias antes de llegar al epsilon final, como en #
# el paper de DeepMind.                                                                                                            #
# Añadido soporte de guardado de epsilon, i y el numero de pasos totales con h5py.                                                 #
# Ahora el programa muestra al final de cada episodio la puntuacion total.                                                         #
#                                                           09/03/2019                                                             #
# Implementada la media de los ultimos 10 valores, para tener una idea de la tendencia, ademas de la media.                        #
#                                                           11/03/2019                                                             #
# Arreglado el que no se mostrase la leyenda en la grafica.                                                                        #
# Cambiada la cantidad de experiencias antes de epsilon final a 300000.                                                            #
# Actualizacion de la red objetivo cada 5000 pasos en vez de 10000.                                                                #
# Vuelta a la actualizacion de la red objetivo cada 1000 pasos.                                                                    #
# Vuelta a gamma = 0.95.                                                                                                           #
#                                                           17/03/2019                                                             #
# En vista de los pobres resulados, se aplican los siguientes cambios:                                                             #
# Gamma: de 0.95 a 0.99.                                                                                                           #
# Cantidad de experiencias hasta epsilon = 0.1 1000000.                                                                            #
# Corregido texto grafica.                                                                                                         #
# Eliminado lo de que si pierde una vida done = True.                                                                              #
#                                                           18/03/2019                                                             #
# Implementado repeticion de la accion cada 4 pasos.                                                                               #
# Implementado no-op maximo de 30.                                                                                                 #
#                                                           20/03/2019                                                             #
# Cambiado el tamaño de las imagenes de 80x80 a 105x80.                                                                            #
# Añadido que muestre al final del episodio el valor Q medio.                                                                      #
# Añadido seleccion cada 4 cuadros, hallando el maximo entre el tercero y el cuarto; como en el paper de DeepMind.                 #
# Modificado el calculo del loss.                                                                                                  #
#                                                           21/03/2019                                                             #
# Entrenamiento con Deterministic-v4 ya que el frameskip lo hacemos nosotros.                                                      #
# Añadida la puntuacion maxima y el loss medio del episodio en vez del loss final.                                                 #
# Vuelta a la actualizacion de la red objetivo cada 10000 pasos tras leer esto:                                                    #
# https://jaromiru.com/2016/10/21/lets-make-a-dqn-full-dqn/                                                                        #
# Añadido plot de los Q-values medios por episodio.                                                                                #
# Añadido que si pierde una vida, done = True                                                                                      #
# Cambios y mejoras menores en las graficas.                                                                                       #
#                                                          23/03/2019                                                              #
# Eliminamos el frameskipping ya que parece estar implementado de serie en OpenAI-Gym.                                             #
#                                                          26/03/2019                                                              #
# Eliminamos lo de que repita la accion 4 veces ya que parece estar implementada en OpenAI-Gym.                                    #
# Correegimos el no-op; se realiza en los 30 primeros pasos, pero no se limita.                                                    #
# Implementado las recompensas a -1, 0 y 1 como en el paper de DeepMind.                                                           #
#                                                          27/03/2019                                                              #
# Cambiado el best_action_value cuando es aleatorio a 0.                                                                           #
# El agente realizara acciones aleatorias durante el periodo de observacion en vez de utilizar la CNN cuando va aprendiendo.       #
# Cambio del ratio de aprendizaje de 0.00025 para realizar pruebas.                                                                #
# Cambiado np.max por np.argmax en el calculo de los Q values en el entrenamiento.                                                 #
# Cambiado que muestre 6 decimales en vez de 4 en la Q-mean.                                                                       #
#                                                         28/03/2019                                                               #
# Añadido que analice experiencia por experiencia para ver si done == True.                                                        #
# Cambiadas las representaciones de la mayoria de datos por puntos pequeños.                                                       #
# Añadido shuffle = False en model.fit en training ya que por defecto parece estar en True.                                        #
# Modificado como se asigna el premio; ahora parece que al menos en Pong la media de Q values si son positivos.                    #
#                                                         29/03/2019                                                               #
# Modificado el como se actualiza la red objetivo.                                                                                 #
# Modificado four_batch: ahora lo hacemos mediante un deque, eliminando la experiencia mas antigua.                                #
# Modificado no-op para que el numero de experiencias aleatorias sea entre 4 y 30 al emepzar una partida.                          #
#                                                         31/03/2019                                                               #
# Eliminada la normalizacion de imagenes (dividir entre 255). Los Q values son normales ahora.                                     #
# Cambiado como se declaran las CNN: los activadores relu ahora estan en la misma capa.                                            #
# Movido el decrecimiento de epsilon a la seleccion de accion.                                                                     #
#                                                        01/04/2019                                                                #
# Cambiado lr = 0.00001.                                                                                                           #
# Añadido para Pong que si su puntuacion baja de 0, done == True.                                                                  #
#                                                        10/04/2019                                                                #
# Añadido contador de tiempo para saber las horas que tarda el entrenamiento.                                                      #
# Añadida de nuevo la normalizacion de imagenes.                                                                                   #
# Añadida activacion lineal en la ultima capa de la CNN (no deberia afectar).                                                      #
# Corregido error en el que metiamos el estado cuando hay que introducir el nuevo estado en four_batcher.                          #
# Eliminada la condicion de reward >= 0 en Pong para obtener la maxima recompensa.                                                 #
# Cambiado el descenso de epsilon de 1000000 a 300000.                                                                             #     
# TO-DO: Añadir metricas de tensorboard.                                                                                           #  
# TO-DO: Arreglar la grafica                                                                                                       #
# TO-DO: Meter toda la metrica en una funcion por limpieza                                                                         #
# TO-DO: Hacer que compruebe el numero de GPUs, para que la IA pueda entrenarse por defecto en cualquier ordenador.                #   
####################################################################################################################################
# https://github.com/hridayns/Reinforcement-Learning/blob/master/Atari/models/DDQN.py
# 1. IMPORTS Y FROMS:
import gym
import cv2
import numpy as np
import random
import matplotlib.pyplot as plt
import os
import time
import datetime
#import psutil
import h5py
from collections import deque
from keras.layers import Dense, Flatten, Conv2D
from keras.models import Sequential
from keras.utils import multi_gpu_model
from keras.optimizers import Adam

# 2. VARIABLES INICIALES:
game = "PongDeterministic-v4"
logs = "logs/"
date = str(datetime.datetime.now().strftime('%d-%m-%Y-%H:%M:%S'))
start_time = time.time()
env = gym.make(game)
env.reset()
steps =  0          
total_reward = 0
i = 0
i_update = i
episodes_sum = 0
exp_batch = []
weights = "/weights.best.hdf5"
mem_h5 = "/data.hdf5"
save_path = "Save/"
filepath = save_path+game+weights
save_filepath = save_path+game+mem_h5
med_sup = 0
max_reward = 0
l10 = deque((), maxlen = 10)
Q_values = deque()
observation_episodes = 50
update_network = 10000

# 2.1 Creamos los directorios donde guardaremos todo; si ya existe lo dejamos 
# como esta.
try: 
    os.makedirs(save_path+game)
    print("Directory", save_path+game, "created.")
except FileExistsError:
    print("Directory ", save_path+game, "already exists. Variables would be loaded from there.")

try: 
    os.makedirs(save_path+game+logs)
    print("Directory", save_path+game+logs, "created.")
except FileExistsError:
    print("Directory ", save_path+game+logs, "Logs directory exists.")
    
# 2.2 Comprobamos si el fichero con las variables existe; si no, lo creamos:        
if os.path.isfile(save_filepath) is False:
    savedata = h5py.File(save_filepath, 'w')
    savedata.close()        
    print("Restoration point created.")
else:
    print("Restoration point found.")
    
# 3. AI: Aqui definiremos el agente como tal; especificaremos cada seccion del
# agente, explicando que realiza cada parte del mismo.
class AI:
# 3.1 Variables iniciales; estas variables estan disponibles para accederse 
# desde cualquier punto del programa escribiendo AI.nombre_de_la_variable.
# Ejemplo: AI.four_counter    
#    four_counter = 0
    four_batch_size = 4
    four_batch = deque((), maxlen = four_batch_size)
    four_batch_state = deque((), maxlen = four_batch_size)
    mem_size = 100000 
    mem_batch = deque((), maxlen = mem_size)
    replay_sample_size = 32 
    sample_batch = []
    s_batch = []
    cnn_shape = (105,80,4)
    batch_shape = (1,105,80,4)
    learning_rate = 0.00001
    action_size = env.action_space.n
    initial_epsilon = 1
    final_epsilon = 0.1
    epsilon_frames = 300000
    epsilon_decay = (initial_epsilon-final_epsilon)/epsilon_frames
    epsilon = initial_epsilon
    loss_list = []
    gamma = 0.99
    loss_plt = 0
    
# 3.2 Variables de inicio: estas variables son comunes dentro de la clase AI, 
# es decir, del agente.
    def __init__(self):
        self.parallel_model = self.CNN()
        self.parallel_target_model = self.target_CNN()
        
# Aqui intentamos cargar el valor de epsilon en caso de que exista; si no es 
# asi, cargamos el valor inicial de epsilon.        
        try:
            savedata = h5py.File(save_filepath, 'r')
            saved_epsilon = savedata['epsilon']
            AI.epsilon = saved_epsilon[()]
            savedata.close()
            print("\033[1;32;38mExploration rate sucessfully loaded")
        except:
            AI.epsilon = AI.epsilon
            savedata.close()
            print("\033[1;31;38mError: exploration rate values cant be loaded")
            
# 3.3 Definimos la red neuronal convolucional; se ha añadido el procesamiento
# multi-GPU:            
    def CNN(self):
        model = Sequential()
        model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        model.add(Flatten())
        model.add(Dense(512, activation='relu'))
        model.add(Dense(AI.action_size, activation='linear'))
        parallel_model = multi_gpu_model(model, gpus=2)
        parallel_model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        parallel_model.summary()
        print("Model CNN construction complete")
        return parallel_model
                   
# 3.4 Definimos la red neuronal objetivo; debe ser identica a la normal.        
    def target_CNN(self):
        target_model = Sequential()
        target_model.add(Conv2D(32, (8, 8), strides=(4,4), input_shape = AI.cnn_shape, activation='relu'))
        target_model.add(Conv2D(64, (4, 4), strides=(2,2), activation='relu'))
        target_model.add(Conv2D(64, (3, 3), strides=(1,1), activation='relu'))
        target_model.add(Flatten())
        target_model.add(Dense(512, activation='relu'))
        target_model.add(Dense(AI.action_size, activation='linear'))
        parallel_target_model = multi_gpu_model(target_model, gpus=2)
        parallel_target_model.compile(loss="mse", optimizer=Adam(lr=AI.learning_rate))
        parallel_target_model.summary()
        print("Target CNN construction complete")
        print("\033[1;32;38mCNN construction completed sucessfully")
        return parallel_target_model
                 
# 3.5 Con esta funcion, cargamos los valores de los pesos en la red neuronal;
# de esta forma, podremos retomar el entrenamiento en caso de que paremos el
# programa:        
    def load(self):
        model = self.parallel_model
        try:
            model.load_weights(filepath)
            print("\033[1;32;38mModel weights sucessfully loaded")
            print("\033[1;31;38mALL SYSTEMS NOMINAL")
        except:
            print("\033[1;31;38mError: Model values cant be loaded\033[1;37;38m")
            
# 3.6 En esta funcion, guardamos los valores de epsilon, los pesos de las 
# neuronas y el numero de episodios para posteriormente cargarlos en caso de 
# que paremos el programa. Guardamos cada 10 episodios.              
    def save(self, i, steps):
        model = self.parallel_model
        exploration_rate = AI.epsilon
        if episodes % 10 == 0:
            model.save(filepath)
            print("\033[1;37;38mModel saved")
            savedata = h5py.File(save_filepath, 'w')
            saved_epsilon = savedata.create_dataset('epsilon', data = AI.epsilon)
            saved_i = savedata.create_dataset('i', data = i_update)
            saved_steps = savedata.create_dataset('steps', data = step_counter)
            savedata.close()
        return exploration_rate, saved_epsilon, saved_i, saved_steps
            
# 3.7 Con esta funcion copiamos los pesos de la red neuronal principal a la red
# neuronal objetivo; este proceso se realiza cada determinado numero de pasos.
    def update_net(self):
        model = self.parallel_model
        model_weights = model.get_weights()
        target_model = self.parallel_target_model
        target_model.set_weights(model_weights)
        print("Target model sucessfully updated")  
        
# 3.8 en la funcion batcher, creamos un paquete especifico con los estados para
# la seleccion de la accion.
    def batcher(four_batch_state):
        AI.s_batch = []
        AI.s_batch = np.asarray(AI.four_batch_state)
        AI.s_batch.shape = AI.batch_shape
        
# 3.9 En esta funcion creamos los paquetes de cuatro experiencias; cuando estan
# hechos los paquetes de 4, los enviamos a las diferentes funciones del
# programa para su posterior procesamiento.        
    def four_batcher(self, exp_batch):           
        AI.four_batch_state.append(exp_batch[2])
        if len(AI.four_batch_state) == AI.four_batch_size:
            AI.batcher(AI.four_batch_state)
            
# 3.10 En la funcion memory almacenamos todas las experiencias en paquetes de 4
# listas para su posterior procesamiento. El limite de memoria esta definido 
# por la variable AI.mem_size; esto es importante ya que dependiendo de la ram
# de la que dispongamos, el limite se debera ajustar. Cuando se llena, la 
# funcion automaticamente elimina el elemento mas a la izquierda, es decir, el
# elemento mas antiguo (AI.mem_batch.popleft())                               
    def memory(self, exp_batch):        
        if len(AI.mem_batch) < AI.mem_size:
            AI.mem_batch.append(exp_batch)
        else:
            AI.mem_batch.popleft()
                        
# 3.11 En la funcion replay_sampler, cogemos las experiencias acumuladas desde
# la memoria, y las procesamos para prepararlas para introducirlas en el
# entrenamiento; hacemos una seleccion aleatoria de entre los elementos de la
# memoria, dependiendo del tamaño de la misma; si es menor que lo especificado
# en AI.replay_sample_size, tomamos una muestra del tamaño de la propia memoria
# (es decir, todas las muestras); si el tamaño de memoria es mayor, tomamos un
# numero de muestras del tamaño establecido en AI.replay_sample_size.            
    def replay_sampler(self, mem_batch):
        AI.sample_batch = []
        AI.replay_state_mini = [] 
        AI.replay_action_mini = [] 
        AI.replay_next_state_mini = [] 
        AI.replay_reward_mini = [] 
        AI.replay_done_mini = []
        AI.replay_batch = []
        AI.replay_batch_size = 4
        AI.replay_batch_counter = 0
        
        if len(AI.mem_batch) < AI.replay_sample_size:
            AI.sample_batch.append(random.sample(AI.mem_batch, len(AI.mem_batch)))
        else:
            AI.sample_batch.append(random.sample(AI.mem_batch, AI.replay_sample_size))
            
        AI.sample_batch = np.asarray(AI.sample_batch)
        AI.sample_batch.shape = (AI.sample_batch.shape[1],6)          
        for k in range(len(AI.sample_batch)):
            if AI.replay_batch_counter < AI.replay_batch_size:
                AI.replay_state_mini.append(AI.sample_batch[k,0])
                AI.replay_action_mini.append(AI.sample_batch[k,1])
                AI.replay_next_state_mini.append(AI.sample_batch[k,2])
                AI.replay_reward_mini.append(AI.sample_batch[k,3])
                AI.replay_done_mini.append(AI.sample_batch[k,4])
                AI.replay_batch_counter += 1
            if AI.replay_batch_counter == AI.replay_batch_size:
                AI.replay_batch.append((AI.replay_state_mini, AI.replay_action_mini, AI.replay_next_state_mini, AI.replay_reward_mini, AI.replay_done_mini))
                AI.replay_batch_counter = 0
                AI.replay_state_mini = [] 
                AI.replay_action_mini = [] 
                AI.replay_next_state_mini = [] 
                AI.replay_reward_mini = []
                AI.replay_done_mini = []
        AI.replay_batch = np.asarray(AI.replay_batch)
                
# 3.12 En action_selection, como su nombre indica, seleccionamos una accion;
# para ello, usamos un paquete de 4 experiencias procesado anteriormente en la
# funcion batcher; este paquete lo enviamos a la red neuronal principal.
# Despues comparamos el valor de epsilon (nuestro ratio de exploracion) con un
# numero aleatorio. Si el valor aleatorio es menor que el valor de epsilon, 
# tomaremos una accion aleatoria en vez de la calculada con la CNN.                         
    def action_selection(self, s_batch):
        random_value = np.random.rand()
        if random_value < AI.epsilon or i < observation_episodes:
            best_action = env.action_space.sample()
            best_action_value = 0
        else:
            action_values = self.parallel_model.predict(s_batch)
            best_action = np.argmax(action_values[0])
            best_action_value = action_values[0, best_action]
# Decrementamos epsilon hasta su valor minimo:            
        if i > observation_episodes:    
            if AI.epsilon > AI.final_epsilon:
                AI.epsilon -= AI.epsilon_decay
        return best_action, best_action_value

# 3.13 En la funcion training, usamos las experiencias acumuladas anteriormente
# para entrenar la red; aqui se implementa el algoritmo Q-Learning propiamente
# dicho. Tambien se calcula el loss, que es la diferencia entre el estado
# actual y el estado predicho en un futuro; esta medida nos da una idea de cuan
# preciso esta siendo nuestro proceso.                
    def training(self, replay_batch):
        AI.training_s_batch = []
        AI.training_a_batch = []
        AI.training_ns_batch = []
        AI.training_r_batch = []
        AI.training_d_batch = []
        Q_values = []
        loss_batch = []
        batch_size = len(AI.replay_batch)
        Q_values = np.zeros((batch_size, AI.action_size))
        new_Q = np.zeros((batch_size, AI.action_size))
        replay_batch = np.asarray(replay_batch)
        
        for m in range(batch_size):
            AI.training_s_batch = replay_batch[m,0]
            AI.training_a_batch = replay_batch[m,1]
            AI.training_ns_batch = replay_batch[m,2]            
            AI.training_r_batch = replay_batch[m,3]
            AI.training_d_batch = replay_batch[m,4]
            AI.training_s_batch = np.concatenate(AI.training_s_batch)
            AI.training_ns_batch = np.concatenate(AI.training_ns_batch)
            loss_batch.append(AI.training_s_batch)
            
            Q_values[m] = self.parallel_model.predict(AI.training_s_batch.reshape(AI.batch_shape))
            Q_values[m,[item[0] for item in AI.training_a_batch]] = AI.training_r_batch
# Si ha perdido una vida, se le penaliza; de esta forma se "fuerza" al agente a
# que aprenda a no morir. Para penalizarlo no se le dan mas recompensas si ha 
# perdido una vida. Para Pong, penalizamos si su puntuacion es negativa:
            if 'Pong' in game:
#                if reward >= 0:
                    new_Q[m] = self.parallel_target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, [item[0] for item in AI.training_a_batch]] += (AI.gamma * np.max(new_Q))                    
            else: 
                if np.all(AI.training_d_batch) == False:
                    new_Q[m] = self.parallel_target_model.predict(AI.training_ns_batch.reshape(AI.batch_shape))
                    Q_values[m, [item[0] for item in AI.training_a_batch]] += (AI.gamma * np.max(new_Q))
                          
        loss = self.parallel_model.fit(np.asarray(loss_batch).reshape(batch_size,105,80,4), Q_values, batch_size=batch_size, verbose=0, shuffle=False)
        loss_history = loss.history['loss'][0]
        AI.loss_list.append(loss_history)

        if 'Pong' in game:
            if done == True:
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
        else: 
            if lives == 0:            
                AI.loss_plt = np.mean(AI.loss_list)
                AI.loss_list = []
                
# 4. PROCESAMIENTO DE LA IMAGEN
# En image_processing, cogemos cada una de las imagenes que genera el programa
# y las procesamos para dejarlas listas para el resto de tratamiento de datos.
# El procesamiento consiste en lo siguiente: un cambio de tamaño a 105x80 (la 
# imagen original es de tamaño 210x160), pasar la imagen a blanco y negro y a
# escalar los valores de los pixeles a 1 al dividir entre 255.
def image_processing(image):
    processed_image = cv2.resize(image, (105,80))
    processed_image = cv2.cvtColor(processed_image, cv2.COLOR_RGB2GRAY)
    processed_image = processed_image/255.0
    return processed_image

# Esta pequeña funcion nos dara el tiempo de ejecucion conforme vaya avanzando:
def elapsed_time(start_time, episode_time):
    execution_time = episode_time-start_time
    m, s = divmod(execution_time, 60)
    h, m = divmod(m, 60)
    h = int(h)
    m = int(m)
    s = int(s)
    return h, m, s

# 5. BUCLE DE PROGRAMA
# 5.1 Le damos un nombre a nuestro agente; esto es necesario porque ya estamos
# fuera de la clase. Tambien definimos el numero de episodios iniciales. Tras
# esto, intentamos cargar el numero de episodios, asi como los pesos de las
# neuronas con agent.load().
agent = AI() 
episodes = 100010
try:
        savedata = h5py.File(save_filepath, 'r')
        saved_i = savedata['i']
        i_update = saved_i[()]
        savedata.close()
        episodes = episodes-i_update
        print("\033[1;32;38mEpisodes sucessfully loaded")
except:
        episodes = 100010
        savedata.close()
        print("\033[1;31;38mEpisodes.txt could not be loaded. Starting at episode 0")
try:
    savedata = h5py.File(save_filepath, 'r')
    saved_steps = savedata['steps']
    step_counter = saved_steps[()]
    savedata.close()
    print("\033[1;32;38mSteps sucessfully loaded")
except:
    step_counter = 0
    savedata.close()
    print("\033[1;31;38mSteps could not be loaded. Starting at step 0")             
       
agent.load()

# 5.2 Bucle de entrenamiento: aqui entrenamos el programa por un numero de
# episodios determinados; dentro de cada episodio, ponemos un limite de pasos
# (acciones) que puede realizar antes de acabar el episodio. El episodio
# tambien puede acabar porque el agente fracasa y no consigue el objetivo.
# Puesto que para que el resto del proceso funcione, si acabamos de arrancar el
# programa, bien sea por primera vez o tras apagarlo, todos los buffers estaran
# vacios, lo cual provocaria un error (no hay ninguna imagen que procesar, ni
# ningun batch que introducir a las CNN). Para solucinarlo, tomamos el primer
# estado (el estado 0) y lo introducimos un numero de veces suficiente como 
# para que el programa funcione adecuadamente. Desde el bucle tambien se 
# monitoriza el avance del mismo, tanto viendo como progresa visualmente
# env.render(), como creando una grafica (reward_plot); tambien mostramos el
# valor de epsilon para darnos una idea de cuanto ha avanzado el programa.
# Otras metricas que se toman, a nivel de debug, es el tamaño de AI.mem_batch,
# ya que si se nos desborda puede bloquear el ordenador, por eso, al menos en
# las primeras sesiones es recomendable controlar el tamaño de memoria.
# Para ver la ram libre, asi como el uso de la CPU (aunque al usar
# procesamiento en paralelo con la GPU es secundario), usamos las utilidades
# proporcionadas por la libreria psutil (psutil.cpu_percent y 
# psutil.virtual_memory). Se muestra por pantalla el episodio en el que esta y
# el numero de pasos que lleva del mismo cada 100 pasos, como control.      
for i in range (episodes):
    state = env.reset()
    total_reward = 0   
    if i % 10 == 0:
        agent.save(i_update, AI.epsilon)        
    if i == 0 and steps == 0:
        random_action = env.action_space.sample()
        state1, reward1, done1, lives1 = env.step(random_action)
        done1 = False
        action1 = [random_action, 0.0]
        lives1 = lives1['ale.lives']
        next_state1 = state1
        exp_batch = (image_processing(state1), action1, image_processing(next_state1), reward1, done1, lives1)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        agent.four_batcher(exp_batch)
        reward1 = np.sign(reward1)
        total_reward += reward1
        del state1, action1, next_state1, reward1, done1, exp_batch, random_action
    no_op_counter = 0
    no_op_max = np.random.randint(4,30, dtype=int)
    
    for steps in range (10000):
# NO-OP al inicio, como en el paper de DeepMind:
        if no_op_counter < no_op_max:
            action = [0, 0.0]
            no_op_counter += 1
        else:
            action = agent.action_selection(AI.s_batch)
        Q_values.append(action[1])
        next_state, reward, done, lives = env.step(action[0])
        lives = lives['ale.lives']
        reward = np.sign(reward)
# Si pierde una vida, penalizamos evitando que consiga recompensa maxima; asi 
# lo "enseñamos" a no perder vidas.  
        if (lives < lives1):
            done == True
        total_reward += reward
        exp_batch = (image_processing(state), action, image_processing(next_state), reward, done, lives)        
        agent.four_batcher(exp_batch)
        agent.memory(exp_batch)
        state = next_state        
        exp_batch = []
        step_counter += 1
        env.render()
        if i >= observation_episodes:
            agent.replay_sampler(AI.mem_batch)
            agent.training(AI.replay_batch)       
            if step_counter % update_network == 0:
                agent.update_net()
# En Pong, las partidas acaban cuando reward == -1; por eso tenemos que adecuar esto:               
            if 'Pong' in game:
                if done == True:
                  i_update += 1
                  episodes_sum += total_reward
                  episode_time = time.time()
                  if len(l10) < 10:
                      l10.append(total_reward)
                  if total_reward > max_reward:
                    max_reward = total_reward
                  else:
                    l10.popleft()
                    l10.append(total_reward)
                  l10_mean = np.mean(l10)
                  Q_values_mean = np.mean(Q_values)
                  Q_values = []
                  med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                  h,m,s = elapsed_time(start_time, episode_time)
                  if med_episodes > med_sup:
                      med_sup = med_episodes
                  print("\033[1;32;38mEpisode: {}/{}, Total time: {}, Total Score: {}, e: {:.4f}, loss: {:.6f}, running time: {}:{}:{}"
                      .format(i, episodes, steps, total_reward, agent.epsilon, AI.loss_plt, h,m,s))
                  print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}, Q-values mean: {:.6f}" .format(max_reward, med_sup, med_episodes, l10_mean, Q_values_mean))
#                  print("CPU usage: ", psutil.cpu_percent())
#                  print("\033[1;37;38mMemory length: ", len(AI.mem_batch))
#                  print(psutil.virtual_memory())
# Ploteamos las recompensas y el loss para comprobar como funciona todo:
                  plt.subplot(3,1,1)
                  plt.plot(i, AI.loss_plt, 'b*')
                  plt.title('Loss, reward and Q-values vs episodes')
                  plt.ylabel('Loss', fontsize=12)
                  plt.subplot(3,1,2)
                  plt.plot(i, total_reward, 'ro', label = 'Reward')
                  plt.plot(i, med_episodes, 'g.', label = 'Mean')
                  plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
                  plt.xlabel('Episodes', fontsize=12)
                  plt.ylabel('Reward', fontsize=12)
                  plt.subplot(3,1,3)
                  plt.plot(i, Q_values_mean, '.m')
                  plt.xlabel('Episodes', fontsize=12)
                  plt.ylabel('Q-values', fontsize=12)
                  if i == observation_episodes:
                      plt.legend(loc='upper right', bbox_to_anchor=(1.05,0.10), prop={'size':5.5})
                  plt.pause(0.05)
                  plt.show()
                  break
# Si es cualquier otro juego:            
            else:
                if lives == 0:
                    i_update += 1
                    episodes_sum += total_reward
                    episode_time = time.time()
                    if len(l10) < 10:
                        l10.append(total_reward)
                    if total_reward > max_reward:
                        max_reward = total_reward
                    else:
                        l10.popleft()
                        l10.append(total_reward)                        
                    l10_mean = np.mean(l10)
                    Q_values_mean = np.mean(Q_values)
                    Q_values = []
                    med_episodes = round((episodes_sum/((i+1)-observation_episodes)),2)
                    h,m,s = elapsed_time(start_time, episode_time)
                    if med_episodes > med_sup:
                        med_sup = med_episodes
                    print("\033[1;32;38mEpisode: {}/{}, Total time: {}, Total Score: {}, e: {:.4f}, loss: {:.6f}, running time: {}:{}:{}"
                      .format(i, episodes, steps, total_reward, agent.epsilon, AI.loss_plt, h,m,s))
                    print("\033[1;32;38mMax score: {:.2f}, Maximum mean: {:.2f}, Actual mean: {:.2f}, L10 mean: {:.2f}, Q-values mean: {:.6f}" .format(max_reward, med_sup, med_episodes, l10_mean, Q_values_mean))
#                   print("CPU usage: ", psutil.cpu_percent())
#                   print("\033[1;37;38mMemory length: ", len(AI.mem_batch))
#                   print(psutil.virtual_memory())
# Ploteamos las recompensas y el loss para comprobar como funciona todo:
                    plt.subplot(3,1,1)
                    plt.plot(i, AI.loss_plt, 'b.')
                    plt.title('Loss, reward and Q-values vs episodes')
                    plt.ylabel('Loss', fontsize=12)
                    plt.subplot(3,1,2)
                    plt.plot(i, total_reward, 'ro', label = 'Reward')
                    plt.plot(i, med_episodes, 'g.', label = 'Mean')
                    plt.plot(i, l10_mean, 'y.', label = 'L10 Mean')
                    plt.xlabel('Episodes', fontsize=12)
                    plt.ylabel('Reward', fontsize=12)
                    plt.subplot(3,1,3)
                    plt.plot(i, Q_values_mean, '.m')
                    plt.xlabel('Episodes', fontsize=12)
                    plt.ylabel('Q-values', fontsize=12)
                    if i == observation_episodes:
                        plt.legend(loc='upper right', bbox_to_anchor=(1.05,0.10), prop={'size':5.5})
                    plt.pause(0.05)
                    plt.show()
                    break
        if steps % 100 == 0:
                    print("\033[1;37;38mEpisode: {}/{}, Time: {}, Total steps: {}, Score: {}"
                        .format(i, episodes, steps, step_counter, total_reward))
# Adecuamos aqui de nuevo para Pong:
        if 'Pong' in game:
            if done == True:
                break
        else:
            if lives == 0:
                break